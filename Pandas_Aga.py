#!/usr/bin/env python
# coding: utf-8

# In[2]:


import numpy as np
import pandas as pd


# In[3]:


df=pd.read_csv('banklist.csv')
df


# In[10]:


df.head(5)


# In[9]:


df.columns


# In[13]:


df['ST'].nunique()


# In[14]:


df['ST'].unique()


# In[18]:


a=df['ST'].value_counts()
a.head(5)


# In[22]:


a=df['Acquiring Institution'].value_counts()
a.head(5)


# In[23]:


b=df[df['Acquiring Institution']=='State Bank of Texas']
b


# In[51]:


newdf=df[df['City']=='Los Angeles']
b=newdf.groupby('City').count()
b


# In[84]:


newdf=df[~df["Bank Name"].str.contains(pat="Bank")]
len(newdf.index)


# In[94]:


newdf=df[df['Bank Name'].astype(str).str[0]=="S"]
len(newdf.index)


# In[98]:


newdf=df[df['CERT']>20000]
len(newdf.index)


# In[110]:


newdf=df['Bank Name'].str.count(' ')
newdf1=newdf[newdf==1]
len(newdf1)


# In[117]:


newdf=df[df['Closing Date'].str.contains(pat="08")]
len(newdf.index)

