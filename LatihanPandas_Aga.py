#!/usr/bin/env python
# coding: utf-8

# In[70]:


import numpy as np
import pandas as pd


# In[2]:


labels =['a','b','c']
my_list=[10,20,30]
arr=np.array([10,20,30])
d={'a':10,'b':20,'c':30}


# In[3]:


pd.Series(data=my_list)


# In[5]:


pd.Series(data=my_list,index=labels)


# In[6]:


pd.Series(my_list,labels)


# In[7]:


pd.Series(arr)


# In[8]:


pd.Series(arr,labels)


# In[9]:


pd.Series(d)


# In[10]:


pd.Series(data=labels)


# In[11]:


pd.Series([sum,print,len])


# In[12]:


ser1=pd.Series([1,2,3,4],index=['USA','Germany','USSR','Japan'])


# In[13]:


ser1


# In[14]:


ser2=pd.Series([1,2,5,4],index=['USA','Germany','Italy','Japan'])


# In[15]:


ser2


# In[16]:


ser1['USA']


# In[17]:


ser1+ser2


# In[72]:


import pandas as pd
import numpy as np


# In[71]:


from numpy.random import randn
np.random.seed(101)


# In[21]:


df=pd.DataFrame(randn(5,4),index='A B C D E'.split(),columns='W X Y Z'.split())


# In[22]:


df


# In[23]:


df['W']


# In[24]:


df[['W','Z']]


# In[25]:


df.W


# In[26]:


type(df['W'])


# In[27]:


df['new']=df['W']+df['Y']


# In[28]:


df


# In[30]:


df.drop('new',axis=1)


# In[31]:


df


# In[32]:


df.drop('new',axis=1,inplace=True)


# In[33]:


df


# In[35]:


df.drop('E',axis=0)


# In[36]:


df.loc['A']


# In[37]:


df.iloc[2]


# In[38]:


df.loc['B','Y']


# In[40]:


df.loc[['A','B'],['W','Y']]


# In[41]:


df


# In[42]:


df>0


# In[43]:


df[df>0]


# In[44]:


df[df['W']>0]


# In[45]:


df[df['W']>0]['Y']


# In[47]:


df[df['W']>0][['Y','X']]


# In[48]:


df[(df['W']>0) & (df['Y']>1)]


# In[49]:


df


# In[50]:


df.reset_index()


# In[51]:


newind='CA NY WY OR CO'.split()


# In[52]:


df['States']=newind


# In[53]:


df


# In[54]:


df.set_index('States')


# In[55]:


df


# In[56]:


df.set_index('States',inplace=True)


# In[57]:


df


# In[60]:


outside=['G1','G1','G1','G2','G2','G2']
inside=[1,2,3,1,2,3]
hier_index=list(zip(outside,inside))
hier_index=pd.MultiIndex.from_tuples(hier_index)


# In[61]:


hier_index


# In[62]:


df=pd.DataFrame(np.random.randn(6,2),index=hier_index,columns=['A','B'])
df


# In[63]:


df.loc['G1']


# In[64]:


df.loc['G1'].loc[1]


# In[65]:


df.index.names


# In[74]:


df.index.names=['Group','Num']


# In[75]:


df


# In[76]:


df.xs('G1')


# In[77]:


df.xs(['G1',1])


# In[78]:


df.xs(1,level='Num')


# In[79]:


import numpy as np
import pandas as pd


# In[80]:


df = pd.DataFrame({'A':[1,2,np.nan],
                  'B':[5,np.nan,np.nan],
                  'C':[1,2,3]})


# In[81]:


df


# In[82]:


df.dropna()


# In[83]:


df.dropna(axis=1)


# In[84]:


import pandas as pd
data = {'Company':['GOOG','GOOG','MSFT','MSFT','FB','FB'],
       'Person':['Sam','Charlie','Amy','Vanessa','Carl','Sarah'],
       'Sales':[200,120,340,124,243,350]}


# In[86]:


df=pd.DataFrame(data)


# In[87]:


df


# In[88]:


df.groupby('Company')


# In[89]:


by_comp=df.groupby("Company")


# In[90]:


by_comp.mean()


# In[91]:


df.groupby('Company').mean()


# In[92]:


by_comp.std()


# In[93]:


by_comp.min()


# In[94]:


by_comp.max()


# In[95]:


by_comp.count()


# In[97]:


by_comp.describe()


# In[98]:


by_comp.describe().transpose()


# In[100]:


by_comp.describe().transpose()['GOOG']


# In[103]:


import pandas as pd


# In[104]:


df1=pd.DataFrame({'A':['A0','A1','A2','A3'],
                 'B': ['B0','B1','B2','B3'],
                 'C':['C0','C1','C2','C3'],
                 'D':['D0','D1','D2','D3']},
                index=[0,1,2,3])


# In[107]:


df2=pd.DataFrame({'A':['A4','A5','A6','A7'],
                 'B': ['B4','B5','B6','B7'],
                 'C':['C4','C5','C6','C7'],
                 'D':['D4','D5','D6','D7']},
                index=[4,5,6,7])


# In[108]:


df3=pd.DataFrame({'A':['A8','A9','A10','A11'],
                 'B': ['B8','B9','B10','B11'],
                 'C':['C8','C9','C10','C11'],
                 'D':['D8','D9','D10','D11']},
                index=[8,9,10,11])


# In[109]:


df1


# In[110]:


df2


# In[111]:


df3


# In[112]:


pd.concat([df1,df2,df3])


# In[115]:


pd.concat([df1,df2,df3],axis=1)


# In[118]:


left=pd.DataFrame({'key':['K0','K1','K2','K3'],
                  'A':['A0','A1','A2','A3'],
                  'B':['B0','B1','B2','B3']})
right=pd.DataFrame({'key':['K0','K1','K2','K3'],
                  'C':['C0','C1','C2','C3'],
                  'D':['D0','D1','D2','D3']})


# In[119]:


left


# In[120]:


right


# In[122]:


pd.merge(left,right,how='inner',on='key')


# In[127]:


left=pd.DataFrame({'key1':['K0','K0','K1','K2'],
                   'key2':['K0','K1','K0','K1'],
                  'A':['A0','A1','A2','A3'],
                  'B':['B0','B1','B2','B3']})
right=pd.DataFrame({'key1':['K0','K1','K1','K2'],
                    'key2':['K0','K0','K0','K0'],
                  'C':['C0','C1','C2','C3'],
                  'D':['D0','D1','D2','D3']})


# In[128]:


pd.merge(left,right,on=['key1','key2'])


# In[129]:


pd.merge(left,right,how='outer',on=['key1','key2'])


# In[131]:


pd.merge(left,right,how='right',on=['key1','key2'])


# In[132]:


pd.merge(left,right,how='left',on=['key1','key2'])


# In[134]:


left=pd.DataFrame({'A':['A0','A1','A2'],
                  'B':['B0','B1','B2']},
                 index=['K0','K1','K2'])

right=pd.DataFrame({'C':['C0','C1','C2'],
                   'D':['D0','D1','D2']},
                  index=['K0','K2','K3'])


# In[135]:


left.join(right)


# In[136]:


left.join(right,how='outer')


# In[137]:


import pandas as pd
df=pd.DataFrame({'col1':[1,2,3,4],'col2':[444,555,666,444],'col3':['abc','def','ghi','xyz']})
df.head()


# In[138]:


df['col2'].unique()


# In[139]:


df['col2'].nunique()


# In[140]:


df['col2'].value_counts()


# In[141]:


newdf=df[(df['col1']>2)&(df['col2']==444)]


# In[142]:


newdf


# In[143]:


def times2(x):
    return x*2


# In[144]:


df['col1'].apply(times2)


# In[145]:


df['col3'].apply(len)


# In[147]:


df['col1'].sum()


# In[148]:


del df['col1']


# In[149]:


df


# In[150]:


df.columns


# In[151]:


df.index


# In[152]:


df


# In[154]:


df.sort_values(by='col2')


# In[155]:


df.isnull()


# In[156]:


df.dropna()


# In[157]:


import numpy as np


# In[158]:


df =pd.DataFrame({'col1':[1,2,3,np.nan],
                 'col2':[np.nan,555,666,444],
                 'col3':['abc','def','ghi','xyz']})
df.head()


# In[159]:


df.fillna('FILL')


# In[164]:


data={'A':['foo','foo','foo','bar','bar','bar'],
     'B':['one','one','two','two','one','one'],
     'C':['x','y','x','y','x','y'],
     'D':[1,3,2,5,4,1]}

df=pd.DataFrame(data)


# In[2]:


import numpy as np
import pandas as pd


# In[3]:


df=pd.read_csv('example')
df


# In[4]:


df.to_csv('example',index=False)


# In[9]:


pd.read_excel('Excel_Sample.xlsx',sheet_name='Sheet1')


# In[12]:


df.to_excel('Excel_Sample.xlsx',sheet_name='Sheet1')


# In[4]:


df=pd.read_html('http://www.fdic.gov/bank/individual/failed/banklist.html')


# In[5]:


df[0]

